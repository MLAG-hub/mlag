echo Beware that RF is much slower than Logistic Regression, especially with n_estimators=1000

python3 MLAG_main.py SportsChampions_TimePlace.csv ScientistsMedicalDoctors_TimePlace.csv 1 3000 >>log_RF_main.txt
python3 MLAG_main.py SportsChampions_TimePlace.csv ScientistsMedicalDoctors_TimePlace.csv 1 3000 mlag-SpoCha.csv >>log_RF_external_SpoCha.txt
python3 MLAG_main.py ScientistsMedicalDoctors_TimePlace.csv SportsChampions_TimePlace.csv 1 3000 mlag-SciMed.csv >>log_RF_external_SciMed.txt
