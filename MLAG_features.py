#                                                   1/7  1/8  1/9  1/10  1/12    2/5   2/7   2/9    3/7  3/8   4/9   5/12
PARAM=[13, 30, 14, 16, 8, 16,                      0.25,  1,  0.5,  0.5,  1,      1,  0.25, 0.25,  0.25, 0.75, 0.5,  0.75,  #   6+12 = 18
      4.5, 20, 0, 0, 2.5,     1, 1, 2, 8, 3, 1.5,   -1,  -1,  -8,  -11, -1.25,   0.5,  11,   5,     -4,  -12, -0.75,-15.5,  # +11+12 = 41
      5.5, 10, 11,  7, 5,  2.5,   # 47

 8,   9,   4,   5,   4,  2.5,   # Moon orbs for 1/1 ... 1/6
7.5, 4.5,  7,  5.5, 3.5,  1,    # Venus
 7,  3.5,  3,  5.5,  7,  0.75,  # Mars

 0, 3, 2, 1,-1, 0,  2, 2, 1,-1, 2,-8,   4,-99, -1,  0,  3, -4, -2, -2,   # Moon
 2, 1, 9,-3, 8, 4,  3,-2, 1,-3, 0, 2,  -1, -9,  3,-99, -1,  0,  1, -2,   # Venus
 2, 3,-3,-1,-3, 5,  4,10,-2,-2,-12,2,   9, -1,  1, -1,-99,  1, -8,  3 ]  # Mars

signsMoon = len(PARAM)-20*3
signsVenu = len(PARAM)-20*2
signsMars = len(PARAM)-20

hasConjunctions = []
hasExact180or90 = []
def planetsInSigns(longitude, s):
	res = PARAM[s + int((longitude + PARAM[46]) / 30) % 12]
	if len(hasConjunctions)*2 + len(hasExact180or90) >= 4:
		for p in hasConjunctions:  res += PARAM[s + 12 + p]
		for p in hasExact180or90:  res += PARAM[s + 12 + p]
	return res

primary  =  [1, 3, 2, 4, 4,  1, 2, 1,  3, 2]  # 1=plasma, 2=gas, 3=liquid, 4=solid,
secondary = [2, 4, 3, 3, 2,  3, 4, 4,  1, 1]  # aka  Fire, Air, Water, Earth,  see https://en.wikipedia.org/wiki/Classical_element#Western_astrology
def weight(a, b):
	if primary[a]==primary[b]:  	return PARAM[18]	# 4.5
	if primary[a]==secondary[b] and primary[b]==secondary[a]:  return PARAM[19]	# 20
	if primary[a]==secondary[b]:	return PARAM[20]	# 0
	if primary[b]==secondary[a]:	return PARAM[21]	# 0
	return PARAM[22]	# 2.5

def calc_features(longitudes):
	global hasConjunctions, hasExact180or90
	features = []
	for i in [1,3,4]:   # indices of Moon, Venus, Mars
		sum1 = 0
		ii = (0 if i==1 else 1 if i==3 else 2)
		iw = PARAM[ii  ] / 16.0
		io = PARAM[ii+3] / 16.0
		ii = signsMoon-18 + ii * 6
		hasConjunctions = []
		hasExact180or90 = []
		for k in range(8):
			if k==i:  continue
			a = (longitudes[k] - longitudes[i] + 360) % 360  # here you can try +379+k*37 or a similar randomization
			if a > 180:   a = 360 - a
			if   a <= PARAM[ii]:
			                                     hasConjunctions.append(k)
			                                     sum1 += weight(i,k)*PARAM[23]
			elif 180-a <= PARAM[ii+1]:
			                                     if 180-a <= PARAM[ii+1]*PARAM[44] / 32.0:   hasExact180or90.append(k)
			                                     sum1 += weight(i,k)*PARAM[24]
			elif abs(a-360./3)  <= PARAM[ii+2]:
			                                     #if abs(a-360./3)  <= PARAM[ii+2]*1 / 32.0:  hasExact180or90.append(k)
			                                     sum1 += weight(i,k)*PARAM[25]  # 120.0000
			elif abs(a-360./4)  <= PARAM[ii+3]:
			                                     if abs(a-360./4) <= PARAM[ii+3]*PARAM[45] / 32.0:  hasExact180or90.append(k)
			                                     sum1 += weight(i,k)*PARAM[26]  #  90.0000

			elif abs(a-360./5)  <= PARAM[ii+4]:  sum1 += weight(i,k)*PARAM[27]  #  72.0000
			elif abs(a-360./6)  <= PARAM[ii+5]:  sum1 += weight(i,k)*PARAM[28]  #  60.0000

			elif abs(a-360./7)   <=io*PARAM[6]:  sum1 += iw*weight(i,k)*PARAM[29]  #  51.4286
			elif abs(a-360./8)   <=io*PARAM[7]:  sum1 += iw*weight(i,k)*PARAM[30]  #  45.0000
			elif abs(a-360./9)   <=io*PARAM[8]:  sum1 += iw*weight(i,k)*PARAM[31]  #  40.0000
			elif abs(a-360./10)  <=io*PARAM[9]:  sum1 += iw*weight(i,k)*PARAM[32]  #  36.0000
			elif abs(a-360./12)  <=io*PARAM[10]: sum1 += iw*weight(i,k)*PARAM[33]  #  30.0000
			elif abs(a-360.*2/5) <=io*PARAM[11]: sum1 += iw*weight(i,k)*PARAM[34]  # 144.0000
			elif abs(a-360.*2/7) <=io*PARAM[12]: sum1 += iw*weight(i,k)*PARAM[35]  # 102.8571
			elif abs(a-360.*2/9) <=io*PARAM[13]: sum1 += iw*weight(i,k)*PARAM[36]  #  80.0000
			elif abs(a-360.*3/7) <=io*PARAM[14]: sum1 += iw*weight(i,k)*PARAM[37]  # 154.2857
			elif abs(a-360.*3/8) <=io*PARAM[15]: sum1 += iw*weight(i,k)*PARAM[38]  # 135.0000
			elif abs(a-360.*4/9) <=io*PARAM[16]: sum1 += iw*weight(i,k)*PARAM[39]  # 160.0000
			elif abs(a-360.*5/12)<=io*PARAM[17]: sum1 += iw*weight(i,k)*PARAM[40]  # 150.0000

		if i==1:  sum1 += planetsInSigns(longitudes[i], signsMoon) * PARAM[41]
		if i==3:  sum1 += planetsInSigns(longitudes[i], signsVenu) * PARAM[42]
		if i==4:  sum1 += planetsInSigns(longitudes[i], signsMars) * PARAM[43]
		features.append(sum1)
	return features
