for h in {0..23..1}; do (echo $h; python3 printForYears.py 1937 1982 $h >yearsA_hour$h.txt) done
for h in {0..23..1}; do (echo $h; python3 printForYears.py 1937 1957 $h >years1_hour$h.txt) done
for h in {0..23..1}; do (echo $h; python3 printForYears.py 1958 1982 $h >years2_hour$h.txt) done

for h in {0..23..1}; do (
echo $h
python3 MLAG_main.py  SportsChampions_TimePlace.csv ScientistsMedicalDoctors_TimePlace.csv 0 3000 yearsA_hour$h.txt >log-A-hour$h.txt
cat log-A-hour$h.txt | grep "i= 30"
) done

for h in {0..23..1}; do (
echo $h
python3 MLAG_main.py  SportsChampions_TimePlace.csv ScientistsMedicalDoctors_TimePlace.csv 0 3000 years1_hour$h.txt >log-1-hour$h.txt
cat log-1-hour$h.txt | grep "i= 30"
) done

for h in {0..23..1}; do (
echo $h
python3 MLAG_main.py  SportsChampions_TimePlace.csv ScientistsMedicalDoctors_TimePlace.csv 0 3000 years2_hour$h.txt >log-2-hour$h.txt
cat log-2-hour$h.txt | grep "i= 30"
) done
