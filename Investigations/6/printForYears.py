import datetime,sys

yearStart = int(sys.argv[1])
yearEnd   = int(sys.argv[2])
hour      = int(sys.argv[3])
for year in range(yearStart, yearEnd+1):
    for month in range(1,13):
        dt = datetime.datetime(year,month,1, 0,0,0)
        while dt.timetuple()[1]==month:
            day = dt.timetuple()[2]
            print("%d,%d,%d,%d,0,0" % (year, month, day, hour))
            dt += datetime.timedelta(days=1)
