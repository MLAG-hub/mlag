import sys
file = open(sys.argv[1], 'rt')
mode = sys.argv[2]
for line in file:
	if line[0]=='#':  continue
	longitude = line.split(',')[7]
	letter = longitude.strip('0123456789')[0]
	if mode=='E':
		if (letter=='E' or (letter=='W' and int(longitude[:longitude.index(letter)]) < 30)):  print(line.strip())
	if mode=='W':
		if (letter=='W' and int(longitude[:longitude.index(letter)]) >= 30):  print(line.strip())
