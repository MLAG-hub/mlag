import sys, math, ephem
freqs = [0] * 24

file = open(sys.argv[1], 'rt')
for line in file:
	if line[0]=='#':  continue
	line = line.split(',')
	year  = int(line[0])
	month = int(line[1])
	day   = int(line[2])
	hour   = int(line[3])
	minute = int(line[4])
	second = int(line[5])

	julday = ephem.Date((year, month, day, hour, minute, second))
	body = ephem.Sun()
	body.compute(julday, epoch=julday)
	longitude = float( ephem.Ecliptic(body).lon ) / math.pi * 180
	freqs[int(longitude/15)] += 1

print(freqs)

def ev(x):
	return sum(x)*1. / len(x)

def std(x):
	m = ev(x)
	n = [ (xi-m)**2  for xi in x]
	return math.sqrt(ev(n))

s = sum(freqs)
m = ev(freqs)
sd = std(freqs)

i = 0
for x in freqs:
	print("%2d  %3d  %.3f  %.3f  %s" % (i, x, (x-m)*100./s, (x-m)/sd, " " if (-3 <= (x-m)/sd <= 3) else "!!!"))
	i+=1
