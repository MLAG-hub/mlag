python3 extract_Internal_Testing_set.py ../../AG_data/SportsChampions_TimePlace.csv  SC           >../../AG_data/Internal_Testing-SC.csv
python3 extract_Internal_Testing_set.py ../../AG_data/ScientistsMedicalDoctors_TimePlace.csv SMD  >../../AG_data/Internal_Testing-SMD.csv

cat ../../AG_data/Internal_Testing-SC.csv  ../../AG_data/mlag-SpoCha.csv   >../../AG_data/both_Testing-SC.csv
cat ../../AG_data/Internal_Testing-SMD.csv ../../AG_data/mlag-SciMed.csv   >../../AG_data/both_Testing-SMD.csv

# All-SMD.csv is for investigation part 5
cat ../../AG_data/mlag-SciMed.csv ../../AG_data/ScientistsMedicalDoctors_TimePlace.csv   >../../AG_data/All-SMD.csv

python3 sign_frequencies.py ../../AG_data/both_Testing-SC.csv
python3 sign_frequencies.py ../../AG_data/both_Testing-SMD.csv
