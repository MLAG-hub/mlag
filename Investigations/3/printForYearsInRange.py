import datetime,sys

year = int(sys.argv[1])
for month in range(1,13):
		dt = datetime.datetime(year,month,1, 0,0,0)
		while dt.timetuple()[1]==month:
			day = dt.timetuple()[2]
			for hour in range(0, 24):
				print("%d,%d,%d,%d,0,0" % (year, month, day, hour))
			dt += datetime.timedelta(days=1)
