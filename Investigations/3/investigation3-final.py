y = [
0.5190885,
0.5414422,
0.5756137,
0.4744857,
0.6032865,
0.4699356,
0.5789929,
0.5823216,
0.5907084,
0.4850274,
0.5897813,
0.4696930,
0.5284984,
0.4245080,
0.5189526,
0.4035860,
0.5387061,
0.5961718,
0.4785699,
0.5538913,
0.4680093,
0.6302386,
0.4778533,
0.6636148,
0.4730431,
0.6695552,
0.5993387,
0.5377571,
0.4061183,
0.5536693,
0.3479968,
0.5291681,
0.6023567,
0.5314066,
0.7473044,
0.4761532,
0.5128293,
0.4914364,
0.5751138,
0.5221051,
0.6346289,
0.5477840,
0.5837342,
0.3367919,
0.5008665,
0.4421382,
]
x = range(1937,1983)
so = sorted(y)
print('min and max: ', so[0], x[y.index(so[0])], so[-1], x[y.index(so[-1])]) 

import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score

# Create linear regression object
regr = linear_model.LinearRegression()

y = np.asarray(y).reshape(-1,1)
x = np.asarray(x).reshape(-1,1)

# Train the model
regr.fit(x, y)

# Make predictions
y_pred = regr.predict(x)
print('Predicted values, first and last: ', y_pred[0], y_pred[-1])

# The coefficients: intercept and slope
print('Coefficients: ', regr.intercept_, regr.coef_)

# The mean squared error
print('Mean squared error: %.5f' % mean_squared_error(y, y_pred))

# The coefficient of determination: 1 is perfect prediction
print('Coefficient of determination: %.5f' % r2_score(y, y_pred))

# Plot outputs
plt.scatter(x, y,  color='black')
plt.plot(x, y_pred, color='blue', linewidth=3)
plt.plot(x, [0.5]*len(x), color='black', linewidth=1)
plt.show()
