from MLAG_features import calc_features
import sys, math, datetime, sklearn, numpy as np  # The Parameter Optimization process had  sklearn 0.21.2  numpy 1.16.4
import ephem   # 3.7.6.0      # pip install pyephem      # https://pypi.python.org/pypi/pyephem
use_swisseph = 0
if use_swisseph:
	import swisseph as swe  # 2.0.0.post2  # pip install pyswisseph   # https://pypi.python.org/pypi/pyswisseph
	swe.set_ephe_path('/usr/share/ephe')  # This folder must have files .se1 from ftp://ftp.astro.com/pub/swisseph/ephe/
from sklearn.utils        import shuffle
from sklearn.linear_model import LogisticRegression     # scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html
from sklearn.ensemble     import RandomForestClassifier # scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html

randomSeed = 321		# You should check other seeds, and randomization below, right above calc_features()
np.random.seed(randomSeed)

files = [  'SportsChampions_TimePlace.csv',           # Group A
           'ScientistsMedicalDoctors_TimePlace.csv',  # Group B
]
UseRF               = False
maxIterations       = 10000  # The main loop will be executed 10'000 times
if len(sys.argv) > 1:  files[0] = sys.argv[1]
if len(sys.argv) > 2:  files[1] = sys.argv[2]
if len(sys.argv) > 3:  UseRF = (sys.argv[3][0]=='T' or int(sys.argv[3])!=0)
if len(sys.argv) > 4:  maxIterations = int(sys.argv[4])
if len(sys.argv) > 5:  files.append(sys.argv[5])   # External Testing set. For accuracy > 0.5 these persons should on average be closer to group A than to group B 

print(str(datetime.datetime.utcnow())[:19], "  sklearn %s  numpy %s  pyephem %s  randomSeed =" % (sklearn.__version__, np.__version__, ephem.__version__), randomSeed, '\n')

longitudes = [0.0]*20	# Ecliptic longitudes of Sun, Moon, Mercury, Venus, Mars, Jupiter, Saturn, Uranus
sourceSets = []	# The n-th element is a table with features of persons in files[n]: rows are persons and columns are features
freqs      = []
for filename in files:
	file = open("AG_data/" + filename, 'rt')
	sourceSet = []
	freq = np.zeros(2020)
	for line in file:
		if line[0]=='#':  continue
		line = line.split(',')
		year  = int(line[0])
		month = int(line[1])
		day   = int(line[2])
		hour   = int(line[3])  # Please make sure the negative hours are OK in your version of pyephem or pyswisseph or whatever,
		minute = int(line[4])  # for example, compare the julday's (see below) for '1973,12,15,-1,25,0' and '1973,12,14,23,25,0'
		second = int(line[5])
		freq[year] += 1

		index = 0
		if use_swisseph:
			julday = swe.julday(year, month, day, hour + minute/60.0 + second/3600.0)
			for body in [swe.SUN, swe.MOON, swe.MERCURY, swe.VENUS, swe.MARS, swe.JUPITER, swe.SATURN, swe.URANUS]:
				longitudes[index] = swe.calc_ut(julday, body, swe.FLG_SWIEPH + swe.FLG_TRUEPOS + swe.FLG_NOABERR + swe.FLG_NOGDEFL)[0]
				index += 1
		else:
			julday = ephem.Date((year, month, day, hour, minute, second))
			for body in [ephem.Sun(), ephem.Moon(), ephem.Mercury(), ephem.Venus(), ephem.Mars(), ephem.Jupiter(), ephem.Saturn(), ephem.Uranus()]:
				body.compute(julday, epoch=julday)
				longitudes[index] = float( ephem.Ecliptic(body).lon ) / math.pi * 180
				index += 1

		#for i in range(len(longitudes)): longitudes[i] = (longitudes[i] + 379 + i*37) % 360   # Randomization of features
		sourceSet.append([year] + calc_features(longitudes))

	sourceSets.append(sourceSet)
	freqs.append(freq)
	file.close()

numFeatures = len(sourceSets[0][0]) - 1  # Excluding year
featureNames = [
"Moon", "Venus", "Mars"
]
assert len(featureNames) == numFeatures  and  maxIterations > 2

if 1:  # Extract the Testing Set
	te0size = te1size = 0
	years0 = {}
	years1 = {}
	for y in range(1500, 2020):  # FirstYear, LastYear+1
		f0 = int(freqs[0][y])
		f1 = int(freqs[1][y])
		if f0>0 and f1==0:
			te0size+= f0
			years0[y]= 1
		if f1>0 and f0==0:
			te1size+= f1
			years1[y]= 1
	#print (te0size, te1size)
	#print (years0.keys())  # Print them to extract the Internal Testing set 1
	#print (years1.keys())  # Print them to extract the Internal Testing set 2
	TeX = np.zeros((te0size+te1size, numFeatures+1))
	TeY = np.ravel([([0]*te0size) + ([1]*te1size)])
	teIdx = 0

	traIdx = 0
	new0 = np.zeros((len(sourceSets[0]) - te0size, numFeatures+1))
	for item in sourceSets[0]:
		year = int(item[0])
		if year in years0:
				TeX[teIdx] = item
				teIdx+=1
		else:
				new0[traIdx] = item
				traIdx += 1
	assert teIdx == te0size and traIdx == len(sourceSets[0]) - te0size
	sourceSets[0] = new0

	traIdx = 0
	new1 = np.zeros((len(sourceSets[1]) - te1size, numFeatures+1))
	for item in sourceSets[1]:
		year = int(item[0])
		if year in years1:
				TeX[teIdx] = item
				teIdx+=1
		else:
				new1[traIdx] = item
				traIdx += 1
	assert teIdx == te0size+te1size and traIdx == len(sourceSets[1]) - te1size
	sourceSets[1] = new1

if len(files) > 2:  # if External Testing set
	TeX = np.asarray(sourceSets[2])
	TeY = np.ravel([0]*len(sourceSets[2]))

def YearBalancedSplit(setA, setB, freqsA, freqsB, maxTra):
	data1 = shuffle(setA)   # Note this is a random shuffle, that's
	data2 = shuffle(setB)   #                                why we need many iterations
	freq0 = np.minimum(freqsA, freqsB)
	if sum(freq0) > maxTra:  freq0 = np.round(freq0 * (maxTra * 1.0 / sum(freq0)))
	trainingSetSize = int(sum(freq0))  # Half size actually
	validatSet1size = len(data1) - trainingSetSize
	validatSet2size = len(data2) - trainingSetSize
	X  = np.zeros((trainingSetSize*2,               numFeatures+1))
	Xv = np.zeros((validatSet1size+validatSet2size, numFeatures+1))
	y  = np.ravel([([0]*trainingSetSize) + ([1]*trainingSetSize)])
	yv = np.ravel([([0]*validatSet1size) + ([1]*validatSet2size)])
	freq1  = np.copy(freq0)
	freq2  = np.copy(freq0)
	trnIdx = valIdx = 0
	for item in data1:
		year = int(item[0])
		if freq1[year] > 0:
				freq1[year]-=1
				X[trnIdx] = item
				trnIdx+=1
		else:
				Xv[valIdx] = item
				valIdx += 1
	assert trnIdx==trainingSetSize and valIdx==validatSet1size
	for item in data2:
		year = int(item[0])
		if freq2[year] > 0:
				freq2[year]-=1
				X[trnIdx] = item
				trnIdx+=1
		else:
				Xv[valIdx] = item
				valIdx += 1
	assert trnIdx==trainingSetSize*2 and valIdx==validatSet1size+validatSet2size and validatSet1size==yv.shape[0]-sum(yv) and validatSet2size==sum(yv)
	return X, y, Xv, yv

sumTrainingSetAccuracy = sumValidationSetAccuracy = sumv1 = sumv2 = sumTeSA = sumt1 = sumt2 = 0
featureCoefs = np.zeros(numFeatures)
allVaSA = []
allTeSA = []

Xt = TeX[:, 1:]  # discard year
yt = TeY
testingSet1size = yt.shape[0] - np.sum(yt)
testingSet2size = np.sum(yt)
teSums = [0.0] * yt.shape[0]

for i in range(1, maxIterations+1):
	X, y, Xv, yv = YearBalancedSplit(np.asarray(sourceSets[0]), np.asarray(sourceSets[1]), freqs[0], freqs[1], 1000000)
	validatSet1size = yv.shape[0] - sum(yv)
	X  =  X[:, 1:]  # discard year
	Xv = Xv[:, 1:]  # discard year
	if i==1: print(numFeatures,"features, sizes of Training,Validation,Testing sets: ", X.shape, Xv.shape, validatSet1size, sum(yv),\
				Xt.shape, testingSet1size, testingSet2size, " UseRF =", UseRF, "\nFiles:", str(files)[1:-1], '\n')
	if validatSet1size==0 or sum(yv)==0 or (len(files)<=2 and (testingSet1size==0 or testingSet2size==0)):  sys.exit(1)
	X, y = shuffle(X, y)   # Just in case... actually no reason to shuffle again here?
	if UseRF: classifier = RandomForestClassifier(min_samples_leaf=100, n_estimators=1000)  # ATraSA is close to 100% with the default min_samples_leaf=1
	else:     classifier = LogisticRegression(solver='liblinear')
	classifier.fit(X, y)
	featureCoefs += (classifier.feature_importances_  if UseRF else  classifier.coef_[0])
	accuracy1 = classifier.score(Xv[:validatSet1size], yv[:validatSet1size])  # Validation Set 1
	accuracy2 = classifier.score(Xv[validatSet1size:], yv[validatSet1size:])  # Validation Set 2
	sumv1 += accuracy1
	sumv2 += accuracy2
	sumTrainingSetAccuracy   += classifier.score(X, y)
	sumValidationSetAccuracy += (accuracy1 + accuracy2) / 2
	allVaSA.append((accuracy1 + accuracy2) / 2)
	teSums += classifier.predict(Xt)
	if len(files) > 2:  # if External testing set
		accuracy = classifier.score(Xt, yt)
		sumTeSA += accuracy
		allTeSA.append(accuracy)
		print("i=%5d: AVaSA=%1.7f=(%1.7f+%1.7f)/2" % (i, sumValidationSetAccuracy/i, sumv1/i, sumv2/i), " ATrSA=%1.7f  ATeSA=%1.7f"\
		% (sumTrainingSetAccuracy/i, sumTeSA/i), " VaSA=%1.7f=(%1.7f+%1.7f)/2" % ((accuracy1 + accuracy2)/2, accuracy1, accuracy2))
	else:
		accuracy1 = classifier.score(Xt[:testingSet1size], yt[:testingSet1size])  # Testing Set 1
		accuracy2 = classifier.score(Xt[testingSet1size:], yt[testingSet1size:])  # Testing Set 2
		sumt1 += accuracy1
		sumt2 += accuracy2
		sumTeSA += (accuracy1 + accuracy2) / 2
		allTeSA.append((accuracy1 + accuracy2) / 2)
		print("i=%5d: AVaSA=%1.7f=(%1.7f+%1.7f)/2" % (i, sumValidationSetAccuracy/i, sumv1/i, sumv2/i), " ATrSA=%1.7f  ATeSA=%1.7f"\
		% (sumTrainingSetAccuracy/i, sumTeSA/i), " = (%1.7f+%1.7f)/2" % (sumt1/i, sumt2/i))
	if i % 100 == 0:
		print("\nFeatures and their weights:")
		sortedFeatures = np.int32(np.asarray(sorted(zip(abs(featureCoefs), range(numFeatures))))[:,1])
		for i in reversed(sortedFeatures):
			print("%-8s  %1.3f" % (featureNames[i], featureCoefs[i] * 100. / max(abs(featureCoefs))))
		print()

a1 = a2 = sumTesa = numTesa = 0
for i in range(testingSet1size):
	if teSums[i]*2 <= maxIterations:  a1 += 1
for i in range(testingSet2size):
	if teSums[testingSet1size+i]*2 > maxIterations:  a2 += 1
if testingSet2size==0:  a2,testingSet2size = a1,testingSet1size
print("The Majority Vote TeSA =", (a1 / testingSet1size + a2 / testingSet2size)/2)  # This is helpful when there's just one person in the external Testing set

so = sorted(allVaSA)
meVaSA = so[maxIterations//2] if (maxIterations % 2) else (so[maxIterations//2] + so[maxIterations//2 - 1]) * 0.5
for i in range(maxIterations):
	if allVaSA[i] >= meVaSA:
		sumTesa += allTeSA[i]
		numTesa += 1
print("medianVaSA = %1.7f,  mean TeSA when VaSA is above median = %1.7f.   maxVaSA = %1.7f, that TeSA = %1.7f" % (meVaSA, sumTesa/numTesa, so[-1], allTeSA[allVaSA.index(so[-1])]))
