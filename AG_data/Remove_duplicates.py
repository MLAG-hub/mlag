import sys
dates = []
file = open(sys.argv[1], 'r')

for line in file:
	if line[0]=='#':
		print(line.strip())
		continue
	vars = line.split(',')
	y = str(int(vars[0].strip()))
	o = str(int(vars[1].strip()))
	d = str(int(vars[2].strip()))
	h = str(int(vars[3].strip()))
	m = str(int(vars[4].strip()))
	s = str(int(vars[5].strip()))

	dateTime = y+'_'+o+'_'+d+'_'+h+'_'+m+'_'+s
	if not (dateTime in dates):
		dates.append(dateTime)
		print(line.strip())

file.close()
