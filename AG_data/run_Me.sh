python3 Fetch_from_cura.free.fr.py

python3 Remove_duplicates.py Actors_TimePlace.csv       >_tmp_Actors_TimePlace.csv
python3 Remove_duplicates.py Journalists_TimePlace.csv  >_tmp_Journalists_TimePlace.csv
python3 Remove_duplicates.py Musicians_TimePlace.csv    >_tmp_Musicians_TimePlace.csv
python3 Remove_duplicates.py Painters_TimePlace.csv     >_tmp_Painters_TimePlace.csv
python3 Remove_duplicates.py Politicians_TimePlace.csv  >_tmp_Politicians_TimePlace.csv
python3 Remove_duplicates.py Writers_TimePlace.csv      >_tmp_Writers_TimePlace.csv
cat _tmp_*.csv >merged6_TimePlace.csv

cat _tmp_Painters_TimePlace.csv _tmp_Musicians_TimePlace.csv  >Painters_Musicians.csv
cat _tmp_Actors_TimePlace.csv _tmp_Politicians_TimePlace.csv  >Actors_Politicians.csv
cat _tmp_Writers_TimePlace.csv _tmp_Journalists_TimePlace.csv >Writers_Journalists.csv

# In Sports Champions and Scientists Medical Doctors two are false duplicates,
# those are different people born on the same date at the same time.
# Only one is a true duplicate: Lucien Leger (born on 1912-8-29) in volumes A2 and E1.

mv ScientistsMedicalDoctors_TimePlace.csv _tmp_ScientistsMedicalDoctors_TimePlace.csv
grep -a -v "1912,08,29,1,00,0," _tmp_ScientistsMedicalDoctors_TimePlace.csv >ScientistsMedicalDoctors_TimePlace.csv

mv HeredityVolB_TimePlace.csv    _tmp_HeredityVolB_TimePlace.csv
mv HeredityVolE2_TimePlace.csv   _tmp_HeredityVolE2_TimePlace.csv
mv MilitaryMen_TimePlace.csv     _tmp_MilitaryMen_TimePlace.csv
python3 Remove_duplicates.py _tmp_HeredityVolB_TimePlace.csv     >HeredityVolB_TimePlace.csv
python3 Remove_duplicates.py _tmp_HeredityVolE2_TimePlace.csv    >HeredityVolE2_TimePlace.csv
python3 Remove_duplicates.py _tmp_MilitaryMen_TimePlace.csv      >MilitaryMen_TimePlace.csv

rm  _tmp_*.csv
