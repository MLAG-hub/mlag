import sys
dates = []
startYear  = int(sys.argv[2])

def rewrite_coordinate(x):
	separators = x.strip('0123456789')
	index1 = x.index(separators[0])
	index2 = x.index("'")
	degrees = x[ : index1]
	minutes = x[index1+1 : index2]
	Letter = separators[-1]
	assert Letter in 'NESW'
	return degrees + Letter + minutes

with open(sys.argv[1], 'r', encoding='cp1252') as file:
	for line in file:
		vars = line.split(';')
		_date = vars[1].strip().split('.')
		_time = vars[2].strip().split(':')
		_zone = vars[3].strip().split(':')
		_latitude  = rewrite_coordinate(vars[5].strip())
		_longitude = rewrite_coordinate(vars[6].strip())

		theTime = int(_time[0]) * 3600
		if len(_time) > 1:  theTime += int(_time[1]) * 60
		if len(_time) > 2:  theTime += int(_time[2])

		theZone = int(_zone[0]) * 3600
		if len(_zone) > 1:  theZone += int(_zone[1]) * 60
		if len(_zone) > 2:  theZone += int(_zone[2])

		time = theTime - theZone

		hour   = time // 3600
		minute = (time - hour*3600) // 60
		second =  time - hour*3600 - minute*60

		assert 0<=minute<=59
		assert 0<=second<=59

		y = str(int(_date[2]))
		o = str(int(_date[1]))
		d = str(int(_date[0]))
		h = str(hour)
		m = str(minute)
		s = str(second)

		dateTime = y+','+o+','+d+','+h+','+m+','+s+','+_latitude+','+_longitude
		if not (dateTime in dates):
			dates.append(dateTime)

dates = sorted(dates)
for d in dates:
	y = int(d[0:4])
	if y >= startYear: print(d)
