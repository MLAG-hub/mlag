if [ -f "SADC.exe" ]; then
  echo "Using the local copy of the SADC archive..."
else
wget http://astrozet.net/zip/DBase/SADC.exe
if [ -f "SADC.exe" ]; then
  echo "Downloaded the SADC archive..."
else
wget http://astrosystem.ru/AstroSystem/Main/Research/Base/SADC/sadc.rar
mv sadc.rar SADC.exe
if [ -f "SADC.exe" ]; then
  echo "Downloaded the SADC archive from the 2nd site..."
else
  echo "Failed to find, and then failed to download the SADC archive !"
  exit
fi
fi
fi
unrar x SADC.exe

if [ -f "SADC-A.zbs" ]; then

echo "Starting the main engine..." 
cat SADC-?.zbs >all-SADC.txt
rm SADC-?.zbs
grep -a -E " OLYMPIC |MEDALIST|CHAMPION|; SPORTSM"  all-SADC.txt >zbs-spocha0.txt
grep -a -E "; SCIENTIST|; CHEMIST|; PHYSICIST|; PHYSICIAN|; PHYSIOLOGIST|; SURGEON|, SCIENTIST|, CHEMIST|, PHYSICIST|, PHYSICIAN|, PHYSIOLOGIST|, SURGEON|; ASTROPHYSICIST|; ASTRONOMER|BIOLOGIST|GEOLOGIST" all-SADC.txt >zbs-scimed0.txt
grep -a -v "; NOTED FAMILY" zbs-spocha0.txt >zbs-spocha.txt
grep -a -v "; NOTED FAMILY" zbs-scimed0.txt >zbs-scimed.txt
python3 zbs2mlag.py zbs-spocha.txt 1958 >mlag-spocha0.txt
python3 zbs2mlag.py zbs-scimed.txt 1937 >mlag-scimed0.txt
echo "Excluding the AG outliers..."
grep -a -E -v "1960,5,1,|1960,8,2,|1962,12,12,"   mlag-spocha0.txt >mlag-SpoCha.csv
grep -a -E -v "1938,12,14,|1939,3,5,|1939,10,21," mlag-scimed0.txt >mlag-SciMed.csv
rm mlag-*.txt
rm zbs-*.txt
cp mlag-*.csv ../AG_data/
echo "All done!"

else 
    echo "Failed to extract files from the SADC archive!  No unrar installed?  Try this command: sudo apt-get install unrar"
fi
